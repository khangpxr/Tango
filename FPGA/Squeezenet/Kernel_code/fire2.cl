#include<clc.h>
__kernel void __attribute__ ((reqd_work_group_size(55,1,1)))fire2(__global float *Layer2_pool_HW,__global float *fire2squeeze1x1_Weights_HW, __global float *fire2squeeze1x1_Features,__global float *fire2expand1x1_Weights_HW,__global float *fire2expand3x3_Weights_HW,__global float *fire2_Features)
{
	float Features1 = 0;
	float Features2 = 0;
	float Features3 = 0;
	int x = get_local_id(0);
	int y = get_group_id(0);

	for(int f=0; f<16; f++)
		{
			Features1 = 0;
			for(int n=0; n<96; n++)
			{
	               		Features1+= Layer2_pool_HW[n*55*55 + x*55 + y]*fire2squeeze1x1_Weights_HW[f*96+n];
			}
			//ReLU activation function computation
			if(Features1<0)
				Features1 = 0;
			fire2squeeze1x1_Features[f*55*55 + x*55 + y] = Features1;
		}

	barrier(CLK_LOCAL_MEM_FENCE);

		for(int f=0; f<64; f++)
		{
			Features2 = 0;
			for(int n=0; n<16; n++)
			{
				float result = 0;
	               		result = fire2squeeze1x1_Features[n*55*55 + x*55 + y]*fire2expand1x1_Weights_HW[f*16+n];
				Features2+= result;
			}
			//ReLU activation function computation
			if(Features2<0)
				Features2 = 0;
			fire2_Features[f*55*55 + x*55 + y] = Features2;
		}
		barrier(CLK_LOCAL_MEM_FENCE);

		fire2_Features=fire2_Features+(55*55*64);

		for(int f=0; f<64; f++)
		{
			Features3 = 0;
			for(int n=0; n<16; n++)
			{	float result = 0;
					for(int i = x-1; i<=x+1; i++)
					{
	    					for(int j=y-1; j<=y+1; j++)
	    					{
							int x_index = i-x+1;
							int y_index = j-y+1;
							int m = (y_index)+(x_index)*3;
	         					if(i<0 || j<0)
							{
								result+=0;
							}
	         					else if(j>54 || i>54)
							{
								result+=0;
							}
	         					else
							{
	               						result+= fire2squeeze1x1_Features[n*55*55 + i*55 + j]*fire2expand3x3_Weights_HW[m+f*9*16+n*9];
							}
						}
					}
					Features3 += result;
			}
			//ReLU activation function computation
			if(Features3<0)
				Features3 = 0;
			fire2_Features[f*55*55 + x*55 + y] = Features3;
		}
}


