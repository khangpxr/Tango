
__kernel void __attribute__ ((reqd_work_group_size(27,1,1)))fire6(__global float *fire5_Features,__global float *fire6squeeze1x1_Weights_HW,__global float *fire6squeeze1x1_Features,__global float *fire6expand1x1_Weights_HW,__global float *fire6expand3x3_Weights_HW,__global float *fire6_Features)
	{
	int x = get_local_id(0);
	int y = get_group_id(0);
	float Features1 = 0;
	float Features2 = 0;
	float Features3 = 0;

	for(int f=0; f<48; f++)
	{
		Features1 = 0;
		for(int n=0; n<256; n++)
		{
               		Features1+= fire5_Features[n*27*27 + x*27 + y]*fire6squeeze1x1_Weights_HW[f*256+n];
		}
		//ReLU activation function computation
		if(Features1<0)
			Features1 = 0;
		fire6squeeze1x1_Features[f*27*27 + x*27 + y] = Features1;

	}


	for(int f=0; f<192; f++)
	{
		Features2 = 0;
		for(int n=0; n<48; n++)
		{
			float result = 0;
               		result = fire6squeeze1x1_Features[n*27*27 + x*27 + y]*fire6expand1x1_Weights_HW[f*48+n];
			Features2+= result;
		}
		//ReLU activation function computation
		if(Features2<0)
			Features2 = 0;
		fire6_Features[f*27*27 + x*27+ y] = Features2;
	}

	fire6_Features=fire6_Features+(27*27*192);

	for(int f=0; f<192; f++)
	{
		Features3 = 0;
		for(int n=0; n<48; n++)
		{	float result = 0;
				for(int i = x-1; i<=x+1; i++)
				{
    					for(int j=y-1; j<=y+1; j++)
    					{
						int x_index = i-x+1;
						int y_index = j-y+1;
						int m = (y_index)+(x_index)*3;
         					if(i<0 || j<0)
						{
							result+=0;
						}
         					else if(j>26 || i>26)
						{
							result+=0;
						}
         					else
						{
               						result+= fire6squeeze1x1_Features[n*27*27 + i*27 + j]*fire6expand3x3_Weights_HW[m+f*9*48+n*9];
						}
					}
				}
				Features3 += result;
		}
		//ReLU activation function computation
		if(Features3<0)
			Features3 = 0;
		fire6_Features[f*27*27 + x*27 + y] = Features3;
	}
}
