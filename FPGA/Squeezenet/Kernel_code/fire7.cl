__kernel void __attribute__ ((reqd_work_group_size(27,1,1))) fire7(__global float *fire7squeeze1x1_Weights_hw, __global float *fire7expand1x1_Weights_hw, __global float *fire7expand3x3_Weights_hw, __global float *fire7squeeze1x1_Features,  __global float *fire6_Features,  __global float *fire7_Features)
		{
	int x = get_local_id(0);
	int y = get_group_id(0);


	float Features = 0;
	for(int f=0; f<48; f++)
	{
		Features = 0;
		for(int n=0; n<384; n++)
		{
			Features+= fire6_Features[n*27*27 + x*27 + y]*fire7squeeze1x1_Weights_hw[f*384+n];
		}
		//ReLU activation function computation
		if(Features<0)
			Features = 0;
		fire7squeeze1x1_Features[f*27*27 + x*27 + y] = Features;
		//printf("%.8f ",Features);
	}

	barrier(CLK_LOCAL_MEM_FENCE);
	for(int f=0; f<192; f++)
	{
		Features = 0;
		for(int n=0; n<48; n++)
		{
			float result = 0;
			result = fire7squeeze1x1_Features[n*27*27 + x*27 + y]*fire7expand1x1_Weights_hw[f*48+n];
			Features+= result;
		}
		//ReLU activation function computation
		if(Features<0)
			Features = 0;
		fire7_Features[f*27*27 + x*27+ y] = Features;
	}

	barrier(CLK_LOCAL_MEM_FENCE);
	fire7_Features = fire7_Features+(27*27*192);
	for(int f=0; f<192; f++)
	{
		Features = 0;
		for(int n=0; n<48; n++)
		{
			float result = 0;
			for(int i = x-1; i<=x+1; i++)
			{
				for(int j=y-1; j<=y+1; j++)
				{
					int x_index = i-x+1;
					int y_index = j-y+1;
					int m = (y_index)+(x_index)*3;
					if(i<0 || j<0)
					{
						result+=0;
					}
					else if(j>26 || i>26)
					{
						result+=0;
					}
					else
					{
						result+= fire7squeeze1x1_Features[n*27*27 + i*27 + j]*fire7expand3x3_Weights_hw[m+f*9*48+n*9];
					}
				}
			}
			Features += result;
		}

		barrier(CLK_LOCAL_MEM_FENCE);
		//ReLU activation function computation
		if(Features<0)
			Features = 0;
		fire7_Features[f*27*27 + x*27 + y] = Features;
	}



		}

